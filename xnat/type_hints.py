from typing import Any, Dict, List, Optional, Tuple, Union

TimeoutType = Optional[Union[float, Tuple[float, float]]]
JSONType = Union[None, int, str, bool, List[Any], Dict[str, Any]]
